import Accordion from "@/components/Accordion";
import Transition from "@/components/Transition";
import { getSurvey } from "@/lib/client";
import styles from "@/styles/Result.module.scss";
import DataStatement from "@/variables/statement.json";
import { NextApiRequest, NextApiResponse } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import {
    FaCloudDownloadAlt,
    FaFacebook,
    FaTwitter,
    FaWhatsapp,
    FaArrowDown,
    FaInstagram,
    FaTelegramPlane,
} from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import {
    EmailShareButton,
    FacebookShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,
} from "react-share";
import { SocialIcon } from "react-social-icons";
import html2canvas from "html2canvas";
import { display } from "html2canvas/dist/types/css/property-descriptors/display";
import Button from "@/components/Button";

type ObjectData = {
    [key: string]: any;
};

type Category = {
    title: string;
    description: string;
};

type Program = {
    title: string;
    description: string;
    qtt: number;
};

function Result() {
    const localLoader = (data: any) => {
        return `${encodeURI(data.src)}`;
    };

    const [loader, setLoader] = useState(0);
    const router = useRouter();
    const pid = router.query.id;
    const [category2, setCategory2] = useState("");
    const [categories, setCategories] = useState<Category[]>([]);
    const [expandCat, setExpandCat] = useState(100);
    const [expandProg, setExpandProg] = useState(100);
    const [count, setCount] = useState("0");
    const [endCount, setEndCount] = useState(100);
    const [sortProgram, setsortProgram] = useState<Program[]>([]);
    const [styleBG, setStyleBG] = useState("");
    const [showDownload, setShowDownload] = useState(false);
    const [kompilasi, setKompilasi] = useState("");

    useEffect(() => {
        let isMounted = true;
        if (count != "100") {
            let start = parseInt(count);
            const end = endCount;
            if (start === end) return;

            let totalmil = 2;
            let incTime = (totalmil / end) * 1000;

            let timer = setInterval(() => {
                start += 1;
                setCount(String(start));
                if (start === end) {
                    if (start === 100) {
                        setTimeout(function () {
                            setLoader(1);
                        }, 2000);
                    }
                    clearInterval(timer);
                }
            }, incTime);
        }
        return () => {
            isMounted = false;
        };
    }, [endCount]);

    useEffect(() => {
        let isMounted = true;
        if (category2 == "") {
            if (router.query.id) {
                getSurvey(router.query.id)
                    .then((result) => {
                        if (result) {
                            const regex = /(<([^>]+)>)/gi;
                            const safeResult = result.content.replace(
                                regex,
                                ""
                            );
                            const statement = JSON.parse(
                                JSON.parse(safeResult).statements
                            );

                            let programs: ObjectData = [];
                            let cats: ObjectData = [];
                            let category: any[] = categories;
                            let programss: any[] = [];

                            statement.forEach((val: any) => {
                                val = JSON.parse(val);
                                if (val != 2) {
                                    if (programs[String(val.p)]) {
                                        programs[String(val.p)] += 1;
                                    } else {
                                        programs[String(val.p)] = 1;
                                    }

                                    if (
                                        cats[
                                            String(val.p) + "c" + String(val.c)
                                        ]
                                    ) {
                                        cats[
                                            String(val.p) + "c" + String(val.c)
                                        ] += 1;
                                    } else {
                                        cats[
                                            String(val.p) + "c" + String(val.c)
                                        ] = 1;
                                    }
                                }
                            });

                            var sortableProgram: any[][] = [];
                            for (var key in programs) {
                                sortableProgram.push([key, programs[key]]);
                            }
                            sortableProgram.sort(function (a, b) {
                                return b[1] - a[1];
                            });

                            sortableProgram.forEach((val) => {
                                const index = val[0];
                                const title = DataStatement[index].title;
                                const description =
                                    DataStatement[index].description;

                                programss.push({
                                    title: title,
                                    description: description,
                                    qtt: val[1],
                                });
                            });

                            var sortable: any[] = [];
                            for (var key in cats) {
                                sortable.push([key, cats[key]]);
                            }

                            sortable.sort(function (a, b) {
                                return b[1] - a[1];
                            });

                            let cek = 0;
                            sortable.forEach((val: any, key: any) => {
                                const split = val[0].split("c");
                                const title =
                                    DataStatement[split[0]].categories[split[1]]
                                        .title;
                                const description =
                                    DataStatement[split[0]].categories[split[1]]
                                        .description;

                                if (
                                    cek == 0 &&
                                    split[0] != sortableProgram[0][0]
                                ) {
                                    cek = 1;
                                    setCategory2(title);
                                }
                                category.push({
                                    title: title,
                                    description: description,
                                });
                            });

                            setCategories(category);
                            setsortProgram(programss);

                            setStyleBG(
                                `/assets/program/${encodeURI(
                                    programss[0].title
                                )}/BG.png`
                            );

                            setEndCount(100);
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        } else {
            return () => {
                isMounted = false;
            };
        }
    }, [router.query.id]);

    const Kompilasi = () => {
        const img = [
            styleBG,
            `/assets/kategori/${encodeURI(category2)}.png`,
            `/assets/program/${encodeURI(sortProgram[0].title)}/Kiri.png`,
            `/assets/program/${encodeURI(sortProgram[0].title)}/Kanan.png`,
        ];
        return (
            <>
                <div className={styles.bg}>
                    <img src={img[0]} />
                </div>
                <Transition
                    styles={styles}
                    className={"upperRight"}
                    type={"zoomIn"}
                    delayIn={0.5}
                >
                    <img src={img[1]} />
                </Transition>
                <Transition
                    styles={styles}
                    className={"downerLeft"}
                    type={"zoomIn"}
                    delayIn={0.7}
                >
                    <img src={img[2]} />
                </Transition>
                <Transition
                    styles={styles}
                    className={"downerRight"}
                    type={"zoomIn"}
                    delayIn={0.9}
                >
                    <img src={img[3]} />
                </Transition>
            </>
        );
    };

    useEffect(() => {
        const el = document.getElementById("download-kompilasi");
        if (el)
            html2canvas(el).then(function (canvas) {
                const imgData = canvas.toDataURL("image/png");
                setKompilasi(imgData);
            });
    }, [showDownload]);

    function Download() {}

    const PreviewDownload = () => {
        const img = [
            styleBG,
            `/assets/kategori/${encodeURI(category2)}.png`,
            `/assets/program/${encodeURI(sortProgram[0].title)}/Kiri.png`,
            `/assets/program/${encodeURI(sortProgram[0].title)}/Kanan.png`,
        ];
        return (
            <div className={styles.previewDownload} id={"download-kompilasi"}>
                <img className={styles.bg} src={"/assets/Template.png"} />
                <div className={styles.kompilasi}>
                    <div className={styles.bg}>
                        <img src={img[0]} />
                    </div>
                    <div className={styles.upperRight}>
                        <img src={img[1]} />
                    </div>
                    <div className={styles.downerLeft}>
                        <img src={img[2]} />
                    </div>
                    <div className={styles.downerRight}>
                        <img src={img[3]} />
                    </div>
                </div>
            </div>
        );
    };

    // const Download = () => {
    //     console.log(document.getElementById('kontener-kompilasi'))
    //     return <></>
    // }

    return (
        <>
            <Head>
                <meta
                    property="og:url"
                    content={`https://kotamasadepan.bukuhariankorona.org/result/?id=${router.query.id}`}
                />
                <meta property="og:title" content="Kota Masa Depan Kita" />
                <meta
                    property="og:description"
                    content="Sampaikan suaramu untuk kota masa depanmu : https://kotamasadepan.bukuhariankorona.org"
                />
                <meta property="og:type" content="website" />
                <meta
                    property="og:image"
                    content="https://kotamasadepan.bukuhariankorona.org/og.jpg"
                />
                <meta
                    property="og:image:secure_url"
                    content="https://kotamasadepan.bukuhariankorona.org/og.jpg"
                />
                <meta
                    property="og:image:url"
                    content="https://kotamasadepan.bukuhariankorona.org/og.jpg"
                />
                <meta
                    property="twitter:image"
                    content="https://kotamasadepan.bukuhariankorona.org/og.jpg"
                />
                <meta
                    property="twitter:description"
                    content="Sampaikan suaramu untuk kota masa depanmu : https://kotamasadepan.bukuhariankorona.org"
                />
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
                <link
                    href="https://fonts.googleapis.com/css2?family=Outfit:wght@100;200&family=Source+Sans+Pro:wght@400;500;600;700&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div
                className={styles.loader}
                style={{ display: loader ? "none" : "" }}
            >
                <h1>{count}%</h1>
            </div>
            {showDownload == true && (
                <div className={styles.modalPreview}>
                    <div className={styles.container_btn}>
                        <a href={kompilasi} download={"kotaku.png"}>
                            <FaCloudDownloadAlt
                                size={30}
                                style={{
                                    marginRight: "5px",
                                }}
                            />
                            Unduh
                        </a>
                    </div>
                    <PreviewDownload />
                    <div
                        className={styles.overlay}
                        onClick={() => setShowDownload(!showDownload)}
                    ></div>
                </div>
            )}
            {loader == 1 && (
                <div style={{ width: "100%" }}>
                    <div className={styles.result_kolase}>
                        <div className={styles.container_title}>
                            <h1>Kotamu di Masa Depan</h1>
                        </div>
                        <div
                            className={styles.kompilasi}
                            id={"kontener-kompilasi"}
                        >
                            <Kompilasi />
                        </div>
                        <p>
                            Ilustrasi di atas adalah gambaran yang
                            merepresentasikan masa depan kotamu berdasarkan
                            beberapa hal yang kamu pedulikan selama ini. Jangan
                            lupa bagi hasil surveimu dan sebut akun pemerintah
                            lokalmu, ya!
                        </p>
                        <div className={styles.share}>
                            <div className={styles.container_title}>
                                <h2>
                                    Bagikan Imajinasi{" "}
                                    <div className={styles.enter} />
                                    Kota Masa Depanmu
                                </h2>
                            </div>
                            <div className={styles.container_btn}>
                                <div className={styles.container_btn_share}>
                                    <FacebookShareButton
                                        url={
                                            "https://kotamasadepan.bukuhariankorona.org/result/?id=" +
                                            router.query.id
                                        }
                                        quote={
                                            "\nHalo, ini adalah gambaran ideal #kotamasadepanku yang aku impikan. Kamu bisa mendapatkan gambar dan menyampaikan pendapat tentang masa depan kota seperti ini dengan klik tautan: "
                                        }
                                        hashtag={"#kotamasadepanku"}
                                        className={styles.btn_share}
                                    >
                                        <FaFacebook
                                            size={35}
                                            style={{ color: "#346498" }}
                                        />
                                    </FacebookShareButton>
                                </div>
                                <div className={styles.container_btn_share}>
                                    <TwitterShareButton
                                        title={
                                            "\nHalo, ini adalah gambaran ideal #kotamasadepanku yang aku impikan. Kamu bisa mendapatkan gambar dan menyampaikan pendapat tentang masa depan kota seperti ini dengan klik tautan: "
                                        }
                                        url={
                                            "https://kotamasadepan.bukuhariankorona.org/result/?id=" +
                                            router.query.id
                                        }
                                        hashtags={[]}
                                        className={styles.btn_share}
                                    >
                                        <FaTwitter
                                            size={35}
                                            style={{ color: "#4580BF" }}
                                        />
                                    </TwitterShareButton>
                                </div>
                                <div className={styles.container_btn_share}>
                                    <WhatsappShareButton
                                        title={
                                            "\nHalo, ini adalah gambaran ideal #kotamasadepanku yang aku impikan. Kamu bisa mendapatkan gambar dan menyampaikan pendapat tentang masa depan kota sepertiku dengan klik tautan di bawah ini, ya!"
                                        }
                                        separator={" : "}
                                        url={
                                            "https://kotamasadepan.bukuhariankorona.org/result/?id=" +
                                            router.query.id
                                        }
                                        className={styles.btn_share}
                                    >
                                        <FaWhatsapp
                                            size={35}
                                            style={{ color: "#45c655" }}
                                        />
                                    </WhatsappShareButton>
                                </div>
                                <div className={styles.container_btn_share}>
                                    <div
                                        className={`${styles.btn_share} ${styles.withIcon}`}
                                        onClick={() => setShowDownload(true)}
                                    >
                                        <FaCloudDownloadAlt
                                            size={30}
                                            style={{
                                                marginRight: "5px",
                                            }}
                                        />
                                        Unduh
                                    </div>
                                </div>
                                <div className={styles.container_btn_share}>
                                    <EmailShareButton
                                        title={"Kotaku di Masa Depan"}
                                        url={
                                            "https://kotamasadepan.bukuhariankorona.org/result/?id=" +
                                            router.query.id
                                        }
                                        subject={"Kotaku di Masa Depan"}
                                        body={
                                            "Hei, lihatlah gambaran ideal #kotamasadepanku!\nSampaikan pendapatmu juga agar aku bisa melihat gambaran ideal masa depan kotamu dengan klik tautan:"
                                        }
                                        className={`${styles.btn_share} ${styles.withIcon}`}
                                    >
                                        <MdEmail
                                            size={30}
                                            style={{
                                                marginRight: "5px",
                                            }}
                                        />
                                        Kirim
                                        <br />
                                        Via Email
                                    </EmailShareButton>
                                </div>
                            </div>
                        </div>
                        <div className={styles.selengkapnya}>
                            <a href="https://kotamasadepan.bukuhariankorona.org">
                                Ikuti Surveinya
                            </a>
                        </div>
                    </div>

                    <div className={styles.result_categories}>
                        <div className={styles.container_title} id={"scroll"}>
                            <h1>
                                Hal Terpenting
                                <br />
                                di Kota Masa Depanmu
                            </h1>
                        </div>
                        <p>
                            Pilihanmu adalah cerminan dari nilai atau hal yang
                            paling kamu pedulikan. Berikut beberapa hal yang
                            kamu prioritaskan di masa depan kotamu.
                        </p>
                        <Accordion
                            expand={expandCat == 0}
                            title={categories[0].title}
                            content={categories[0].description}
                            type={"category"}
                            onClick={() =>
                                setExpandCat(expandCat == 0 ? 100 : 0)
                            }
                            bgColor={"#FFA399"}
                        />
                        <Accordion
                            expand={expandCat == 1}
                            title={categories[1].title}
                            content={categories[1].description}
                            type={"category"}
                            onClick={() =>
                                setExpandCat(expandCat == 1 ? 100 : 1)
                            }
                            bgColor={"#9EE5D2"}
                        />
                        <Accordion
                            expand={expandCat == 2}
                            title={categories[2].title}
                            content={categories[2].description}
                            type={"category"}
                            onClick={() =>
                                setExpandCat(expandCat == 2 ? 100 : 2)
                            }
                            bgColor={"#85ACD5"}
                        />
                        <Accordion
                            expand={expandCat == 3}
                            title={categories[3].title}
                            content={categories[3].description}
                            type={"category"}
                            onClick={() =>
                                setExpandCat(expandCat == 3 ? 100 : 3)
                            }
                            bgColor={"#D19FC4"}
                        />
                    </div>

                    <div className={styles.result_program_container}>
                        <div className={styles.result_program}>
                            <div className={styles.container_title}>
                                <h1>
                                    Seperti Apa Masa
                                    <br />
                                    Depan Kotamu?
                                </h1>
                            </div>

                            {sortProgram.map((val, key) => {
                                const bg = [
                                    "#FFA399",
                                    "#9EE5D2",
                                    "#85ACD5",
                                    "#D19FC4",
                                ];
                                return (
                                    <Accordion
                                        key={key}
                                        expand={expandProg == key}
                                        title={val.title}
                                        content={val.description}
                                        type={"program"}
                                        onClick={() =>
                                            setExpandProg(
                                                expandProg == key ? 100 : key
                                            )
                                        }
                                        bgColor={bg[key]}
                                        percentage={(val.qtt / 20) * 100}
                                    />
                                );
                            })}
                        </div>
                        <div className={styles.copy}>
                            <h2>
                                Masa Depan Kota: Dari, Untuk, dan Oleh Warga
                            </h2>
                            <p>
                                Skenario ini adalah alat bantu visualisasi akan
                                wajah dan masa depan kota yang kau impikan.
                            </p>
                            <p>
                                Hasil dari survei ini diharapkan bisa menjadi
                                pemantik diskusi lanjutan tentang kota di masa
                                depan dan berguna sebagai pertimbangan dalam
                                penyusunan kebijakan yang relevan, sebab suaramu
                                patut didengar.
                            </p>
                            <p>
                                Jadi, seperti apa kota yang kamu bayangkan di
                                masa depan?
                            </p>
                        </div>
                    </div>

                    <div className={styles.thanks}>
                        <div className={styles.container_title}>
                            <h1>
                                Terima Kasih
                                <br />
                                atas Partisipasimu!
                            </h1>
                        </div>
                        <p>
                            Hasil dari pilihanmu merupakan masukan yang penting
                            untuk menciptakan visi bersama terkait program kota
                            di masa depan. Kami akan menjadikan visi bersama ini
                            sebagai rekomendasi kepada pemangku kepentingan
                            terkait langkah kebijakan yang harus diambil untuk
                            masa depan kota di Indonesia.
                        </p>
                    </div>

                    <div className={styles.footer}>
                        <div className={styles.logo_rcus}>
                            <Link href={"https://rujak.org/"}>
                                <a>
                                    <img src={"/assets/logo.png"} />
                                </a>
                            </Link>
                        </div>
                        <div className={styles.sanggare}>
                            <Link href={"https://www.instagram.com/sanggare/"}>
                                <a>Dirancang oleh Sanggare Studio</a>
                            </Link>
                        </div>
                        <div className={styles.iykwim}>
                            <img src={"/assets/iykwim.png"} />
                        </div>
                        <div className={styles.social}>
                            <Link href={"https://twitter.com/rujakrcus"}>
                                <a>
                                    <FaTwitter
                                        size={35}
                                        style={{ color: "#4580BF" }}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className={styles.social}>
                            <Link href={"https://facebook.com/RujakRCUS/"}>
                                <a>
                                    <FaFacebook
                                        size={35}
                                        style={{ color: "#4580BF" }}
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className={styles.social}>
                            <Link href={"https://www.instagram.com/rujakrcus/"}>
                                <a>
                                    <FaInstagram
                                        size={35}
                                        style={{ color: "#e94475" }}
                                    />
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const meta = {
        title: "Result",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Result";
    const pageName = "result";
    const baseUrl = process.env.BASE_URL;
    return {
        props: {
            layout,
            meta,
            baseUrl,
            pageName,
        },
    };
}

export default Result;
