import { GetStaticPaths, NextApiRequest, NextApiResponse } from "next";
import Statements from "@/variables/statement.json";
import Pairs from "@/variables/pair.json";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Button, Transition } from "@/components";
import styles from "@/styles/Statement.module.scss";

type Prop = {
    statements: any;
    pageName: string;
    pid: number;
};

function Statement(props: Prop) {
    const router = useRouter();
    const pid = props.pid;
    const [local, setLocal] = useState([]);
    const [selected, setSelected] = useState(3);

    function btnSelect() {
        if (typeof window !== "undefined") {
            const LocalStorage = localStorage.getItem("statements");
            if (LocalStorage) {
                const localParse = JSON.parse(LocalStorage);
                if (
                    JSON.stringify(props.statements[0].data) ===
                    JSON.stringify(localParse[pid])
                ) {
                    setSelected(0);
                } else if (
                    JSON.stringify(props.statements[1].data) ===
                    JSON.stringify(localParse[pid])
                ) {
                    setSelected(1);
                }
            }
        }
    }

    useEffect(() => {
        btnSelect();
    });

    function setStatement(data: any) {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("statements");
            if (local) {
                const localParse = JSON.parse(local);
                localParse[pid] = data;
                localStorage.setItem("statements", JSON.stringify(localParse));
            } else {
                const localParse = [data];
                localStorage.setItem("statements", JSON.stringify(localParse));
            }
        }

        btnSelect();
    }

    return (
        <>
            <div className={styles.container}
            >
                <Transition
                    key={pid + 0}
                    styles={styles}
                    className={"half"}
                    type="slideUp"
                    exitType={selected == 0 ? "slideUp" : "fade"}
                    delayOut={selected == 0 ? 0.5 : 0}
                >
                    <Button
                        href={pid < 19 ? "/statements/" + (pid + 1) : "/result"}
                        onClick={() => setStatement(props.statements[0].data)}
                        type={"statement"}
                        selected={selected == 0 ? true : false}
                    >
                        {props.statements[0].statement}
                    </Button>
                </Transition>

                <Transition
                    key={pid + 1}
                    styles={styles}
                    className={"half"}
                    type="slideUp"
                    exitType={selected == 1 ? "slideUp" : "fade"}
                    delayOut={selected == 1 ? 0.5 : 0}
                >
                    <Button
                        href={pid < 19 ? "/statements/" + (pid + 1) : "/result"}
                        onClick={() => setStatement(props.statements[1].data)}
                        type={"statement"}
                        selected={selected == 1 ? true : false}
                    >
                        {props.statements[1].statement}
                    </Button>
                </Transition>
                <Transition
                    key={pid + 2}
                    styles={styles}
                    className={"choosent"}
                    type="slideUp"
                    delayIn={5}
                    exitType={selected == 2 ? "slideUp" : "fade"}
                    delayOut={selected == 2 ? 0.5 : 0}
                >
                    <Button
                        href={pid < 19 ? "/statements/" + (pid + 1) : "/result"}
                        onClick={() => setStatement(2)}
                        type={"choosent"}
                        selected={selected == 2 ? true : false}
                    >
                        Tidak bisa memilih
                    </Button>
                </Transition>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const meta = {
        title: "Statement",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Statement";
    const pid = 18;
    const statements = [
        {
            data: JSON.stringify({
                p: Pairs[pid][0].programs,
                c: Pairs[pid][0].category,
                s: Pairs[pid][0].statement,
            }),
            statement:
                Statements[Pairs[pid][0].programs].categories[
                    Pairs[pid][0].category
                ].statement[Pairs[pid][0].statement],
        },
        {
            data: JSON.stringify({
                p: Pairs[pid][1].programs,
                c: Pairs[pid][1].category,
                s: Pairs[pid][1].statement,
            }),
            statement:
                Statements[Pairs[pid][1].programs].categories[
                    Pairs[pid][1].category
                ].statement[Pairs[pid][1].statement],
        },
    ];

    return {
        props: {
            layout,
            meta,
            statements,
            pid,
        },
    };
}

// export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {

//     return {
//         paths: [], //indicates that no page needs be created at build time
//         fallback: 'blocking' //indicates the type of fallback
//     }
// }

export default Statement;
