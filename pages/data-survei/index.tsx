import { getSurveysAPI, isSignedIn, listSurvey } from "@/lib/client";
// import ExportExcel from "@/components/ExportExcel"
import { NextApiRequest, NextApiResponse } from "next";
import Router from "next/router";
import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import DataTable from "react-data-table-component";
import Identity from "@/variables/identity.json";
import Pair  from "@/variables/pair.json";
import Provinces from "@/variables/provinces.json";
import Regency from "@/variables/regencies.json";
import { FaLink, FaExternalLinkAlt } from "react-icons/fa";
import { GrUpdate } from "react-icons/gr"
import Link from "next/link";

import styles from "@/styles/Dashboard.module.scss";
import Head from "next/head";
const { GoogleSpreadsheet } = require('google-spreadsheet');
const doc = new GoogleSpreadsheet(process.env.spreadsheetId);

type TableHead = {
    surveyId: any;
    age: any;
    education: any;
    gender: any;
    profession: any;
    province: any;
    regency: any;
    index: number;
};

type ObjectData = {
    [key: string]: any;
};

function Results(props: any) {
    const [cookie, setCookie] = useCookies(["access_token"]);
    const [data, setData] = useState({
        identity: [
            {
                age: 3,
                education: 4,
                gender: 1,
                profession: 5,
                live: { province: 1, regency: 1, initial: 1 },
            },
        ],
        statements: [
            '{"p":0,"c":1,"s":0}',
            '{"p":3,"c":1,"s":0}',
            '{"p":3,"c":1,"s":1}',
            '{"p":1,"c":0,"s":2}',
            '{"p":1,"c":0,"s":3}',
            '{"p":2,"c":0,"s":3}',
            '{"p":0,"c":1,"s":1}',
            '{"p":3,"c":1,"s":2}',
        ],
    });

    const [dataId, setDataId]: any[] = useState([]);
    const [dataIdentity, setDataIdentity]: any[] = useState([]);
    const [dataStatement, setDataStatement]: any[] = useState([]);
    const [dataTable, setDataTable] = useState([])
    // const doc = new GoogleSpreadsheet(process.env.SPREAD_ID)

    async function getSurveys() {
        const result = await listSurvey(cookie);
        if (result) {
            try {
                const identities: any[] = []
                const statements: any[] = []
                const ids: any[] = []
                result.surveys.nodes.forEach((value: any) => {
                    const identity = JSON.parse(
                        JSON.parse(value.content).identity
                    );
                    const statement = JSON.parse(
                        JSON.parse(value.content).statements
                    );

                    identities.push(identity)
                    statements.push(statement)
                    ids.push(value.id)
                });
                setDataIdentity(identities);
                setDataId(ids);
                setDataStatement(statements);
            } catch (err) {
                console.log(err);
            }
        }
    }

    useEffect(() => {
        getSurveys();
        // getSurveysAPI()
    }, []);

    useEffect(() => {
        setDataTable(() => {
            return dataIdentity.map((val: any, key: any) => {
                const province = Provinces[val.live.province].name;
                const regency: String =
                    props.regencies[Provinces[val.live.province].id][
                        String(val.live.regency)
                    ].name;
                return {
                    index: key + 1,
                    surveyId: dataId[key],
                    age: Identity.age.answer[val.age],
                    education: Identity.education.answer[val.education].replace(
                        "_",
                        " "
                    ),
                    gender: Identity.gender.answer[val.gender],
                    province: province,
                    regency: regency.replace("KABUPATEN", "KAB."),
                    profession: Identity.profession.answer[val.profession].replace(
                        "_",
                        " "
                    ),
                };
            });
        })
    }, [dataIdentity])

    useEffect(() => {
        if (!isSignedIn(cookie)) {
            Router.push("/data-survei/login");
        }
    });

    const columns = [
        {
            name: "No.",
            selector: (row: TableHead) => row.index,
            sortable: true,
            width: "75px",
            center: true
        },
        {
            name: "Provinsi",
            selector: (row: TableHead) => row.province,
            sortable: true,
            compact: true,
            width: "",
            center: false
        },
        {
            name: "Kota/Kab.",
            selector: (row: TableHead) => row.regency,
            sortable: true,
            compact: true,
            width: "200px",
            center: false
        },
        {
            name: "Usia",
            selector: (row: TableHead) => row.age,
            sortable: true,
            width: "75px",
            compact: true,
            center: false
        },
        {
            name: "Pendidikan",
            selector: (row: TableHead) => row.education,
            sortable: true,
            width: "125px",
            compact: true,
            center: false
        },
        {
            name: "Jenis Kelamin",
            selector: (row: TableHead) => row.gender,
            sortable: true,
            width: "",
            compact: true
        },
        {
            name: "Pekerjaan",
            selector: (row: TableHead) => row.profession,
            sortable: true,
            compact: true,
            center: false,
            width: "",
        },
        {
            name: "Link Hasil",
            selector: (row: TableHead) => {
                return (
                    <Link
                        href={
                            "https://kotamasadepan.bukuhariankorona.org/result?id=" +
                            row.surveyId
                        }
                        passHref
                    >
                        <a>
                            <FaLink size={20} className={styles.btnSurvey} />
                        </a>
                    </Link>
                );
            },
            width: "100px",
            center: true,
            compact: false,
            sortable: false
        },
    ];

    async function exportClick() {
        await doc.useServiceAccountAuth({
            client_email: process.env.clientEmail,
            private_key: process.env.clientKey?.replace(/\\n/g, '\n'),
        });
        await doc.loadInfo(); // loads document properties and worksheets
        const sheet = doc.sheetsByIndex[0];
        const rows = await sheet.getRows();
        const rowLength = rows.length
        const ident: any[] = []
        dataTable.forEach(function (val: TableHead) {
            if (val.index > rowLength - 1) {
                let answer = [
                    "=" + val.index,
                    val.province,
                    val.regency,
                    val.age,
                    val.gender,
                    val.education,
                    val.profession,
                    "https://kotamasadepan.bukuhariankorona.org/result?id=" + val.surveyId
                ]
                var total = 0, program1 = 0, program2 = 0, program3 = 0, program4 = 0, program5 = 0
                
                dataStatement[val.index - 1].forEach((val: any, index: any) => {
                    total++
                    if (val != 2) {
                        if (checkPair(val, index) == 1) {
                            answer.push(1)
                            answer.push(0)
                        } else {
                            answer.push(0)
                            answer.push(1)
                        }

                        if (JSON.parse(val).p == 0) {
                            program1++
                        } else if (JSON.parse(val).p == 1) {
                            program2++
                        } else if (JSON.parse(val).p == 2) {
                            program3++
                        } else if (JSON.parse(val).p == 3) {
                            program4++
                        } else if (JSON.parse(val).p == 4) {
                            program5++
                        }
                    } else {
                        answer.push(0)
                        answer.push(0)
                    }
                })

                answer.push((program1 / total) * 100 + "%")
                answer.push((program2 / total) * 100 + "%")
                answer.push((program3 / total) * 100 + "%")
                answer.push((program4 / total) * 100 + "%")
                answer.push((program5 / total) * 100 + "%")
                
                ident.push(answer)
            }
        })
        const row = await sheet.addRows(ident);
    }
    
    function checkPair(data: any, pair: any) {
        data = JSON.parse(data)
        if (Pair[pair][0].programs == data.p) {
            if (Pair[pair][0].category == data.c) {
                if (Pair[pair][0].statement == data.s) {
                    return 1
                }
            }
        }

        return 0
    }

    return (
        <>
            <div className={styles.content}>
                <div className={styles.navbar}>
                    <div className={styles.btnExternal}>
                        <Link href={"https://docs.google.com/spreadsheets/d/1SnH9UU9RitovIcQr7B73N6y4iKQAFcSqPA1lJRi593I"}>
                            <a>
                                <FaExternalLinkAlt size={20} style={{ marginRight: "5px" }} />
                                Data Lengkap
                            </a>
                        </Link>
                    </div>
                    <div className={styles.btnExport} onClick={exportClick}>
                        <GrUpdate size={20} style={{ marginRight: "5px" }} />
                        Perbarui Excel
                    </div>
                </div>
                <div className={styles.table}>
                    <DataTable columns={columns} data={dataTable} pagination />
                </div>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const meta = {
        title: "Survey Results",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Admin";
    const baseUrl = process.env.BASE_URL;
    const regencies: ObjectData = Regency;

    return {
        props: {
            layout,
            meta,
            baseUrl,
            regencies,
        },
    };
}

export default Results;
