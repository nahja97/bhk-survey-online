import { Button, Transition } from "@/components";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Router from "next/router";
import Image from "next/image";
import Logo from "@/assets/logo.png";
import { Auth, isSignedIn } from "@/lib/client";
import { useCookies } from "react-cookie";

import styles from "@/styles/Login.module.scss";

import type { NextApiRequest, NextApiResponse } from "next";

function Login() {
    const [cookie, setCookie] = useCookies([
        "access_token",
        "refresh_token",
    ]);
    const [formData, setFormData] = useState({
        username: "",
        password: "",
    });

    useEffect(() => {
        if(isSignedIn(cookie)) {
            Router.push("/data-survei/");
        }
    }, [cookie])

    function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        const name = e.target.name;
        const value = e.target.value;

        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    }

    async function handleSubmit(e: React.FormEvent) {
        e.preventDefault();
        const input = {
            "username": formData.username,
            "password": formData.password
        }
        // username: "nahja",
        // password: "QEP5Tv0soPFc29iXZC^FrgAf",
        const loginData = {
            input: {
                input
            },
        };
        try {
            const result = await Auth(loginData);
            setCookie("access_token", result.login.authToken, {
                path: "/",
                maxAge: 36000,
            });
            Router.push("/data-survei/");
        } catch (err) {
            console.log(err);
        }
    }
    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={styles.content}>
                <div className={styles.title}
                >
                    <h1>Form Login</h1>
                </div>

                <form className={styles.form} onSubmit={handleSubmit}>
                    <div className={styles.container_input}>
                        <input
                            className={styles.input_text}
                            id="username"
                            name="username"
                            type="text"
                            autoComplete="username"
                            placeholder="Masukkan Username Anda"
                            onChange={handleChange}
                            required
                        />
                        <div className={styles.overlay}></div>
                    </div>
                    <div className={styles.container_input}>
                        <input
                            className={styles.input_text}
                            id="password"
                            name="password"
                            type="password"
                            autoComplete="password"
                            placeholder="Masukkan Password Anda"
                            onChange={handleChange}
                            required
                        />
                        <div className={styles.overlay}></div>
                    </div>
                    <Button type="login" onClick={handleSubmit}>
                        Masuk
                    </Button>
                </form>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const meta = {
        title: "Login Admin",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Admin";
    const baseUrl = process.env.BASE_URL;
    return {
        props: {
            layout,
            meta,
            baseUrl,
        },
    };
}

export default Login;
