/* eslint-disable @next/next/no-page-custom-font */
import { Button, Transition } from "@/components";
import styles from "@/styles/Identity.module.scss";
import Answer from "@/variables/identity.json";
import Head from "next/head";
import React, { useEffect, useState } from "react";

import type { NextApiRequest, NextApiResponse } from "next";
type Props = {
    professions: [];
    pageName: string;
    question: string;
};

function Profession(props: Props) {
    const [selected, setSelected] = useState(null);
    function btnSelect() {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                setSelected(localParse["profession"]);
            }
        }
    }

    useEffect(() => {
        btnSelect();
    });

    function setProfession(profession: number) {
        const data = { profession: profession };

        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                localParse["profession"] = profession;
                localStorage.setItem("identity", JSON.stringify(localParse));
            } else {
                const localParse = { profession: profession };
                localStorage.setItem("identity", JSON.stringify(localParse));
            }
        }

        btnSelect();
    }

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={`${styles.question} ${styles.profession}`}>
                <h1>{props.question}</h1>
            </div>
            <div className={styles.container}>
                <div className={`${styles.answer_container} ${styles.answer_container_prof}`}>
                    {props.professions.map((prop: any, key) => {
                        const text = prop.split("_").length > 1 ? prop.replace('_', '\n') : prop

                        return (
                            <Transition
                                key={key}
                                styles={styles}
                                delayIn={key * 0.05}
                                className={"third-profession"}
                                type="slideUp"
                                exitType={selected == key ? "slideUp" : "fade"}
                                delayOut={selected == key ? .5 : 0}
                            >
                                <Button
                                    href="/identities/live"
                                    onClick={() => setProfession(key)}
                                    type={
                                        selected == key
                                            ? "selected"
                                            : "unselect"
                                    }
                                    pageName={"profession"}
                                >
                                    {text}
                                </Button>
                            </Transition>
                        );
                    })}
                    {/* <Image src="sample" alt="me" width="64" height="64" /> */}
                </div>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const pageName = "profession";
    const meta = {
        title: "Profession Identity",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Identity";
    const professions = Answer.profession.answer;
    const question = Answer.profession.question;
    return {
        props: {
            layout,
            meta,
            pageName,
            professions,
            question,
        },
    };
}

export default Profession;
