/* eslint-disable @next/next/no-page-custom-font */
import { Button, Transition } from "@/components";
import styles from "@/styles/Identity.module.scss";
import Answer from "@/variables/identity.json";
import Head from "next/head";
import React, { useEffect, useState } from "react";

import type { NextApiRequest, NextApiResponse } from "next";
type Props = {
    ages: [];
    question: string;
};

function Age(props: Props) {
    const [selected, setSelected] = useState(null);

    function btnSelect() {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                setSelected(localParse["age"]);
            }
        }
    }

    useEffect(() => {
        btnSelect();
    });

    function setAge(age: number) {
        const data = { age: age };

        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                localParse["age"] = age;
                localStorage.setItem("identity", JSON.stringify(localParse));
            } else {
                const localParse = { age: age };
                localStorage.setItem("identity", JSON.stringify(localParse));
            }
        }

        btnSelect();
    }

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            {/* <Transition
                styles={styles}
                className={"question"}
                exitType={"fade"}
                pageName={"age"}
            >
                <h1>{props.question}</h1>
            </Transition> */}
            <div className={`${styles.question} ${styles.age}`}>
                <h1>{props.question}</h1>
            </div>
            <div className={styles.container}>
                <div className={styles.answer_container}>
                    {props.ages.map((prop, key) => {
                        return (
                            <Transition
                                key={key}
                                styles={styles}
                                delayIn={key * 0.05}
                                className={"third"}
                                type="slideUp"
                                exitType={selected == key ? "slideUp" : "fade"}
                                delayOut={selected == key ? .5 : 0}
                            >
                                <Button
                                    href="/identities/education"
                                    onClick={() => setAge(key)}
                                    type={
                                        selected == key
                                            ? "selected"
                                            : "unselect"
                                    }
                                    pageName={"age"}
                                >
                                    {prop}
                                </Button>
                            </Transition>
                        );
                    })}
                    {/* <Image src="sample" alt="me" width="64" height="64" /> */}
                </div>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const pageName = "age";
    const meta = {
        title: "Age Identity",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Identity";
    const ages = Answer.age.answer;
    const question = Answer.age.question;
    return {
        props: {
            layout,
            meta,
            pageName,
            ages,
            question,
        },
    };
}

export default Age;
