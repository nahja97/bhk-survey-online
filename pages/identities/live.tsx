/* eslint-disable @next/next/no-page-custom-font */
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Image from "next/image";

import { Button, Transition, FormSelect } from "@/components";
import Provinces from "@/variables/provinces.json";
import Regencies from "@/variables/regencies.json";
import styles from "@/styles/Identity.module.scss";

import type { NextApiRequest, NextApiResponse } from "next";

type Props = {
    provinces: [];
    regencies: any;
    pageName: string;
};

type Data = {
    id: string;
    value: string;
};

type ObjectData = {
    [key: string]: any;
};

function Live(props: Props) {
    const [selected, setSelected] = useState(null);
    const [city, setCity] = useState();
    const [formData, setFormData] = useState({
        province: "",
        regency: "",
        initial: 1,
    });

    function getCity() {
        if (formData["province"] !== "") {
            const provinceId = Provinces[parseInt(formData["province"])].id;
            return props.regencies[provinceId].map((val: any, key: any) => {
                return {
                    value: key,
                    label: val.name.toLowerCase().replace(/(^\w{1})|(\s+\w{1})/g, (letter: string) => letter.toUpperCase()),
                };
            });
        } else {
            return [];
        }
    }

    function setLive() {
        if (typeof window !== "undefined" && formData["regency"] !== "") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                localParse["live"] = formData;
                localStorage.setItem("identity", JSON.stringify(localParse));
            } else {
                const localParse = { live: formData };
                localStorage.setItem("identity", JSON.stringify(localParse));
            }
        }
    }

    useEffect(() => {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local)["live"];
                if (localParse) {
                    setFormData({
                        province:
                            localParse["province"] !== undefined
                                ? localParse["province"]
                                : "",
                        regency:
                            localParse["regency"] !== undefined
                                ? localParse["regency"]
                                : "",
                        initial: 1,
                    });
                }
            }
        }
    }, []);

    useEffect(() => {
        if (formData["initial"] == 0) {
            setFormData((prevState) => ({
                ...prevState,
                regency: "",
            }));
        }
    }, [formData["province"]]);

    useEffect(() => {
        setLive();
    }, [formData["regency"]]);

    const BtnNext = (props: any) => {
        const canNext = true;
        const href = "/statements/0";
        if (canNext) {
            if (props.local) {
                if (props.local["regency"] !== "") {
                    return (
                        <Button href={href} type={"btnNextLive"}>
                            <svg
                                width="143"
                                height="164"
                                viewBox="0 0 143 164"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M135.5 77.6699C138.833 79.5944 138.833 84.4056 135.5 86.3301L12.5 157.344C9.16663 159.269 4.99998 156.863 4.99998 153.014L4.99998 10.9859C4.99998 7.13691 9.16667 4.73129 12.5 6.65578L135.5 77.6699Z"
                                    fill="#86C369"
                                    stroke="#2B2B2B"
                                    strokeWidth="10"
                                />
                            </svg>
                        </Button>
                    );
                }
            }
        }

        return <div style={{ display: "none" }}></div>;
    };

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={`${styles.question} ${styles.live}`}>
                <h1>Di Mana Tinggalmu?</h1>
            </div>
            <Transition
                styles={styles}
                className={"container"}
                pageName={props.pageName}
                type={"slideLeft"}
            >
                <div className={styles.answer_container}>
                    <Transition
                        styles={styles}
                        delayIn={0.5}
                        className={"half"}
                        type="slideUp"
                        exitType={"slideUp"}
                        delayOut={1.5}
                    >
                        <FormSelect
                            data={props.provinces}
                            name={"provinces"}
                            onChange={(e: Data) => {
                                if (e != null && e.value != null) {
                                    setFormData((prevState) => ({
                                        ...prevState,
                                        province: e.value,
                                        initial: 0,
                                    }));
                                }
                            }}
                            placeholder={"Pilih Provinsi"}
                            value={parseInt(formData["province"])}
                        />
                    </Transition>
                    {formData["province"] !== "" ? (
                        <Transition
                            styles={styles}
                            delayIn={0.5}
                            className={"half"}
                            type="fade"
                            exitType={"slideUp"}
                            delayOut={1.5}
                            id={styles.city}
                        >
                            <FormSelect
                                data={getCity()}
                                name={"city"}
                                onChange={(e: Data) => {
                                    if (e != null && e.value != null) {
                                        setFormData((prevState) => ({
                                            ...prevState,
                                            regency: e.value,
                                        }));
                                    }
                                }}
                                value={formData["regency"]}
                                placeholder={"Pilih Kota/Kabupaten"}
                            />
                        </Transition>
                    ) : (
                        ""
                    )}
                </div>
            </Transition>
            <BtnNext local={formData} />
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const pageName = "live";
    const meta = {
        title: "Live Identity",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Identity";
    const provinces = Provinces.map((props, key) => {
        let words = props.name
        if (words == "DKI JAKARTA") {
            words = "DKI Jakarta"
        } else if (words == "DI YOGYAKARTA") {
            words = "DI Yogyakarta"
        } else {
            words = words.toLowerCase().replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
        }
        
        return {
            value: key,
            label: words,
        };
    });

    const regencies: ObjectData = Regencies;
    return {
        props: {
            layout,
            meta,
            pageName,
            provinces,
            regencies,
        },
    };
}

export default Live;
