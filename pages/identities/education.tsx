/* eslint-disable @next/next/no-page-custom-font */
import { Button, Transition } from "@/components";
import styles from "@/styles/Identity.module.scss";
import Answer from "@/variables/identity.json";
import Head from "next/head";
import React, { useEffect, useState } from "react";

import type { NextApiRequest, NextApiResponse } from "next";
type Props = {
    educations: [];
    question: string;
};

function Education(props: Props) {
    const [selected, setSelected] = useState(null);

    function btnSelect() {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                setSelected(localParse["education"]);
            }
        }
    }

    useEffect(() => {
        btnSelect();
    });

    function setEducation(education: number) {
        const data = { education: education };

        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                localParse["education"] = education;
                localStorage.setItem("identity", JSON.stringify(localParse));
            } else {
                const localParse = { education: education };
                localStorage.setItem("identity", JSON.stringify(localParse));
            }
        }

        btnSelect();
    }

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={`${styles.question} ${styles.education}`}>
                <h1>{props.question}</h1>
            </div>
            <div className={styles.container}>
                <div className={styles.answer_container}>
                    {props.educations.map((prop, key) => {
                        return (
                            <Transition
                                key={key}
                                styles={styles}
                                delayIn={key * 0.05}
                                className={"half"}
                                type="slideUp"
                                exitType={selected == key ? "slideUp" : "fade"}
                                delayOut={selected == key ? .5 : 0}
                            >
                                <Button
                                    href="/identities/gender"
                                    onClick={() => setEducation(key)}
                                    type={
                                        selected == key
                                            ? "selected"
                                            : "unselect"
                                    }
                                    pageName={"education"}
                                >
                                    {prop}
                                </Button>
                            </Transition>
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const pageName = "education";
    const meta = {
        title: "Education Identity",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Identity";
    const educations = Answer.education.answer;
    const question = Answer.education.question;
    return {
        props: {
            layout,
            meta,
            pageName,
            educations,
            question,
        },
    };
}

export default Education;
