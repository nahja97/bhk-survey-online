/* eslint-disable @next/next/no-page-custom-font */
import { Button, Transition } from "@/components";
import styles from "@/styles/Identity.module.scss";
import Answer from "@/variables/identity.json";
import Head from "next/head";
import React, { useEffect, useState } from "react";

import type { NextApiRequest, NextApiResponse } from "next";
type Props = {
    genders: [];
    pageName: string;
    question: string;
};

function Gender(props: Props) {
    const [selected, setSelected] = useState(null);
    function btnSelect() {
        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                setSelected(localParse["gender"]);
            }
        }
    }

    useEffect(() => {
        btnSelect();
    });

    function setGender(gender: number) {
        const data = { gender: gender };

        if (typeof window !== "undefined") {
            const local = localStorage.getItem("identity");
            if (local) {
                const localParse = JSON.parse(local);
                localParse["gender"] = gender;
                localStorage.setItem("identity", JSON.stringify(localParse));
            } else {
                const localParse = { gender: gender };
                localStorage.setItem("identity", JSON.stringify(localParse));
            }
        }

        btnSelect();
    }

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={`${styles.question} ${styles.gender}`}>
                <h1>{props.question}</h1>
            </div>
            <div className={styles.container}>
                <div className={styles.answer_container}>
                    {props.genders.map((prop, key) => {
                        return (
                            <Transition
                                key={key}
                                styles={styles}
                                delayIn={key * 0.05}
                                className={"half"}
                                type="slideUp"
                                exitType={selected == key ? "slideUp" : "fade"}
                                delayOut={selected == key ? .5 : 0}
                            >
                                <Button
                                    href="/identities/profession"
                                    onClick={() => setGender(key)}
                                    type={
                                        selected == key
                                            ? "selected"
                                            : "unselect"
                                    }
                                    pageName={"gender"}
                                >
                                    {prop}
                                </Button>
                            </Transition>
                        );
                    })}
                    {/* <Image src="sample" alt="me" width="64" height="64" /> */}
                </div>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const pageName = "gender";
    const meta = {
        title: "Gender Identity",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Identity";
    const genders = Answer.gender.answer;
    const question = Answer.gender.question;
    return {
        props: {
            layout,
            meta,
            pageName,
            genders,
            question,
        },
    };
}

export default Gender;
