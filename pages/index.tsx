/* eslint-disable @next/next/no-page-custom-font */
import { Button, Transition } from "@/components";
import Head from "next/head";
import React from "react";
import Image from "next/image";

import styles from "@/styles/Index.module.scss";

import type { NextApiRequest, NextApiResponse } from "next";
import {FaArrowDown} from 'react-icons/fa'
import $ from 'jquery'

function Index(props: any) {
    const localLoader = (data: any) => {
        return `${encodeURI(data.src)}?w=${data.width}&q=${data.quality || 65}`;
    };

    function klikMulai() { 
        $(document).ready(()=>{
            const about = $('#about_survey')
            if (about) {
                let top = about.offset()?.top
                $([document.documentElement, document.body]).animate({
                    scrollTop: (top ? top - 300 : 0)
                }, 1000, 'swing' , function() {
                    setTimeout(() => {
                        const action = $('#action')
                        if (action) {
                            let top = action.offset()?.top
                            $([document.documentElement, document.body]).animate({
                                scrollTop: (top ? top - 100 : 0)
                            }, 1500)
                        }
                    }, 5000)
                })
            }
        })
    }

    function scrollTo(target: number, duration: number) {
        if (duration <= 0) return;
        var difference = target - window.pageYOffset;
        var perTick = difference / duration * 10;

        setTimeout(function() {
            window.scrollTo(0, window.pageYOffset + perTick);
            if (window.pageYOffset === target) return;
            scrollTo(target, duration - 10);
        }, 10);
    }

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
                <link
                    href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@700;900&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <Transition styles={styles} className={"container"} type={"fade"}>
                <div className={styles.left}>
                    <div className={styles.container_title}>
                        <h1 className={styles.title}>
                            Masa Depan Kota Ada di Tangan Kita!
                        </h1>
                    </div>
                    <div className={styles.slider}>
                        <Image
                            className={styles.sliderItem}
                            loader={localLoader}
                            src={"/assets/kategori/Ruang Kota yang Dirancang dengan Baik.png"}
                            alt="ilustrasi survey rcus"
                            width={4096}
                            height={4096}
                            priority
                        />
                    </div>
                    <p>
                        Kota begitu melekat & mempengaruhi kehidupan kita. Sudah
                        saatnya kita turut ambil peran untuk mewujudkan
                        imajinasi kota yang ideal di masa depan.
                    </p>
                </div>
                <div className={styles.right}>
                    <h1 className={styles.title}>
                        Masa Depan Kota Ada di Tangan Kita!
                    </h1>
                    <p className={styles.text_container}>
                        Selamat datang penghuni kota, mari bersama menyuarakan
                        visi kota yang ideal di masa mendatang!
                    </p>
                    <Button href={"#"} type={"home-btn"} onClick={() => klikMulai()}>
                        <FaArrowDown style={{marginRight: "5px"}} />Aku Ikut
                    </Button>
                </div>
            </Transition>
            <Transition
                styles={styles}
                className={"container_about"}
                type={"slideUp"}
                id={"about_survey"}
                inview
            >
                <Transition
                    styles={styles}
                    className={"stiker-left"}
                    type={"slideRight"}
                    delayIn={1}
                    inview
                >
                    <Image
                        loader={localLoader}
                        src={"/assets/kategori/Konektivitas dan Transportasi.png"}
                        alt="ilustrasi survey rcus"
                        width="2046"
                        height="2046"
                    />
                </Transition>
                <div className={styles.container_title}>
                    <h2 className={styles.title}>
                        Tentang Survei Kota Masa Depan
                    </h2>
                </div>
                <p>
                    Jelajahi lima skenario imajinasi kota yang akan memantik
                    percakapan tentang masa depan kota berdasarkan isu dan nilai
                    yang yang kamu percayai—dari keadilan, kesehatan, hingga
                    budaya dan keberagaman. Jadi, bagaimana kota masa depan yang
                    ideal menurutmu?
                </p>
                <Transition
                    styles={styles}
                    className={"stiker-right"}
                    type={"slideLeft"}
                    delayIn={1}
                    inview
                >
                    <Image
                        loader={localLoader}
                        src={"/assets/kategori/Kelestarian Lingkungan Hidup.png"}
                        alt="ilustrasi survey rcus"
                        width="2046"
                        height="2046"
                    />
                </Transition>
            </Transition>

            <div className={styles.container_action} id={"action"}>
                <Transition
                    styles={styles}
                    className={"skenario"}
                    type={"slideUp"}
                    inview
                >
                    Temukan skenario program kota yang sesuai dengan pilihan
                    yang kamu ambil
                </Transition>
                <Transition
                    styles={styles}
                    className={"siap"}
                    type={"slideUp"}
                    delayIn={0.4}
                    inview
                >
                    <p>
                        Sudah siap menjadi bagian dari program masa depan
                        kotamu?
                    </p>
                </Transition>
                <Transition
                    styles={styles}
                    className={"stiker"}
                    type={"slideUp"}
                    delayIn={0.5}
                    inview
                >
                    <Image
                        loader={localLoader}
                        src={"/assets/kategori/Kesehatan.png"}
                        alt="ilustrasi survey rcus"
                        width="2046"
                        height="2046"
                    />
                </Transition>
                <Transition
                    styles={styles}
                    className={"mulai"}
                    type={"slideUp"}
                    delayIn={0.4}
                    inview
                >
                    Klik tombol ‘Mulai’ dan pilih jawaban yang merepresentasikan
                    tentang dirimu dan imajinasi masa depan kotamu. Pilih yang
                    sekiranya cocok dan penting buatmu, ya!
                </Transition>
                <Transition
                    styles={styles}
                    className={"tombol_mulai"}
                    type={"slideUp"}
                    delayIn={1}
                    inview
                >
                    <div className={styles.container_button} id={"tombol_mulai"}>
                        <div>
                            <Button
                                href={"/identities/age"}
                                type={"home-action-btn"}
                            >
                                Mulai
                            </Button>
                        </div>
                    </div>
                </Transition>
            </div>
        </>
    );
}

export async function getStaticProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const meta = {
        title: "Home",
        description: "RCUS Bukuharian Korona Survey Online",
    };
    const layout = "Default";
    const pageName = "intro";
    const baseUrl = process.env.BASE_URL;
    return {
        props: {
            layout,
            meta,
            baseUrl,
            pageName,
        },
    };
}

export default Index;
