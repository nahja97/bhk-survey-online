import { motion, useAnimation } from "framer-motion";
import React, { FC, useEffect } from "react";
import { useInView } from "react-intersection-observer";
import { StylesProps } from "react-select/dist/declarations/src/styles";

type Props = {
    children: any;
    styles: any;
    delayIn?: number;
    delayOut?: number;
    className?: String;
    type?: String;
    exitType?: String;
    pageName?: String;
    layout?: boolean;
    inview?: boolean;
    style?: any;
    id?: any;
};

type VariantsData = {
    [key: string]: any;
};

const Transition: FC<Props> = ({ children, ...pageProps }) => {
    const styles = pageProps.styles;
    const transitionType = pageProps.type ? String(pageProps.type) : "fade";
    const exitTransition = pageProps.exitType
        ? String(pageProps.exitType)
        : "zero";
    const pageName = pageProps.pageName;
    const controls = useAnimation();
    const { ref, inView } = useInView();

    useEffect(() => {
        if (pageProps.inview) {
            if (!inView) {
                controls.start("hidden");
            }
            if (inView) {
                controls.start("show");
            }
        }
    }, [controls, inView, pageName]);

    const exitData: VariantsData = {
        zero: {},
        fade: {
            opacity: 0,
            transition: {
                default: {
                    duration: 0.5,
                    delay: pageProps.delayOut ? pageProps.delayOut : 0,
                },
            },
        },
        slideUp: {
            opacity: 0,
            y: -300,
            transition: {
                default: {
                    duration: 0.5,
                    delay: pageProps.delayOut ? pageProps.delayOut : 0,
                },
            },
        },
        slideDown: {
            opacity: 1,
            y: 300,
            transition: {
                default: {
                    duration: 0.5,
                    // delay: pageProps.delay ? pageProps.delay : 0,
                },
            },
        },
        slideLeft: {
            hidden: {
                opacity: 0,
                transform: "translate(100%, 0)",
            },
            show: {
                opacity: 1,
                transform: "translate(0%, 0)",
                transition: {
                    default: {
                        duration: 2,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
        },
        slideRight: {
            hidden: {
                opacity: 0,
                transform: "translate(-100%, 0)",
            },
            show: {
                opacity: 1,
                transform: "translate(0, 0)",
                transition: {
                    default: {
                        duration: 2,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
        },
    };

    const variantData: VariantsData = {
        fade: {
            hidden: { opacity: 0 },
            show: {
                opacity: 1,
                transition: {
                    default: {
                        duration: 0.5,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
            exit: exitData[exitTransition],
        },
        slideUp: {
            hidden: {
                opacity: 0,
                transform: "translate(0, 50%)",
            },
            show: {
                opacity: 1,
                transform: "translate(0, 0%)",
                transition: {
                    default: {
                        duration: 0.5,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
            exit: exitData[exitTransition],
        },
        slideDown: {
            hidden: {
                y: -100
            },
            show: {
                opacity: 1,
                y: 0,
                transition: {
                    default: {
                        duration: 1,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
            exit: exitData[exitTransition],
        },
        slideLeft: {
            hidden: {
                opacity: 0,
                x: "100%",
            },
            show: {
                opacity: 1,
                x: 0,
                transition: {
                    default: {
                        duration: 0.5,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
            exit: exitData[exitTransition],
        },
        slideRight: {
            hidden: {
                opacity: 0,
                x: "-100%"
            },
            show: {
                opacity: 1,
                x: 0,
                transition: {
                    default: {
                        duration: 0.5,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            },
            exit: exitData[exitTransition],
        },
        zoomIn: {
            hidden: {
                scale: 0.5,
                opacity: 0
            },
            show: {
                scale: 1,
                opacity: 1,
                transition: {
                    default: {
                        duration: .5,
                        delay: pageProps.delayIn ? pageProps.delayIn : 0,
                    },
                },
            }
        }
    };

    if (pageProps.inview) {
        return (
            <motion.div
                layout={pageProps.layout}
                variants={variantData[transitionType]}
                initial="hidden"
                animate={controls}
                exit="exit"
                className={`${styles[String(pageProps.className)]} ${
                    pageName ? styles[String(pageName)] : ""
                }`}
                id={pageProps.id ? pageProps.id : ''}
                ref={ref}
            >
                {children}
            </motion.div>
        );
    } else {
        return (
            <motion.div
                layout={pageProps.layout}
                variants={variantData[transitionType]}
                initial="hidden"
                animate="show"
                exit="exit"
                className={`${styles[String(pageProps.className)]} ${
                    pageName ? styles[String(pageName)] : ""
                }`}
                style={pageProps.style}
            >
                {children}
            </motion.div>
        );
    }
};

export default Transition;
