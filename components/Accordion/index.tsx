import { motion } from "framer-motion";
import React, { Component, Fragment, FC, useState, useEffect } from "react";
import style from "./Accordion.module.scss";

type Props = {
    title: string;
    content: string;
    type: string;
    expand?: boolean;
    onClick?: any;
    bgColor?: string;
    percentage?: number;
};

const Accordion: FC<Props> = ({ children, ...pageProps }) => {
    const type = pageProps.type;
    const title = pageProps.title;
    const content = pageProps.content;

    if (type == "category") {
        return (
            <>
                <div className={style.container}>
                    <div className={style.title}>
                        <div
                            className={style.container_title}
                            style={{ backgroundColor: pageProps.bgColor }}
                        >
                            <h2>{title}</h2>
                            <div
                                className={`${style.btn} ${
                                    pageProps.expand ? style.expand : ""
                                }`}
                                onClick={pageProps.onClick}
                            >
                                <div className={style.btn1}></div>
                                <div className={style.btn2}></div>
                            </div>
                        </div>
                    </div>
                    <div
                        className={`${style.content} ${
                            pageProps.expand ? style.expand : ""
                        }`}
                    >
                        <p>{content}</p>
                    </div>
                </div>
            </>
        );
    } else {
        return (
            <>
                <div className={`${style.container} ${style.program}`}>
                    <div className={style.title}>
                        <div
                            className={style.container_title}
                        >
                            <div className={style.container_title_percentage}>
                                <div className={style.container_percentage}>
                                <div
                                    className={style.percentage}
                                    style={{
                                        backgroundColor: pageProps.bgColor,
                                    }}
                                >
                                    {pageProps.percentage}%
                                </div>
                                </div>
                                <h2>{title}</h2>
                            </div>
                            <div
                                className={`${style.btn} ${
                                    pageProps.expand ? style.expand : ""
                                }`}
                                onClick={pageProps.onClick}
                            >
                                <div className={style.btn1}></div>
                                <div className={style.btn2}></div>
                            </div>
                        </div>
                    </div>
                    <div
                        className={`${style.content} ${
                            pageProps.expand ? style.expand : ""
                        }`}
                    >
                        <p>{content}</p>
                    </div>
                </div>
            </>
        );
    }
};

export default Accordion;
