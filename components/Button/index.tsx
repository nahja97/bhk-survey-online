import Link from "next/link";
import React, { Children, FC } from "react";

import styles from "./Button.module.scss";

type Props = {
    type?: String;
    children: any;
    href?: String;
    data?: any;
    selected?: boolean;
    onClick?: any;
    passHref?: boolean;
    pageName?: String;
    classname?: String;
};

const Button: FC<Props> = ({ children, ...pageProps }) => {
    if (pageProps.type == "login") {
        return (
            <button
                type="submit"
                className={`${styles.container} ${
                    pageProps.type ? styles[String(pageProps.type)] : ""
                } ${pageProps.selected ? styles.selected : ""}`}
                onClick={pageProps.onClick}
            >
                <div className={styles.overlay}></div>
                <div className={`${styles.content}`}>{children}</div>
            </button>
        );
    } else {
        return (
            <Link
                href={String(pageProps.href)}
                passHref={pageProps.passHref ? pageProps.passHref : true}
            >
                <div
                    className={`${styles.container} ${
                        pageProps.type ? styles[String(pageProps.type)] : ""
                    } ${pageProps.selected ? styles.selected : ""} ${pageProps.classname ? styles[String(pageProps.classname)] : ""}`}
                    onClick={pageProps.onClick}
                >
                    <div className={styles.overlay}></div>
                    <div
                        className={`${styles.content} ${
                            pageProps.pageName
                                ? styles[String(pageProps.pageName)]
                                : ""
                        }`}
                    >
                        {children}
                    </div>
                </div>
            </Link>
        );
    }
};

export default Button;
