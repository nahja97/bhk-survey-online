import { motion } from "framer-motion";
import React, { Component, Fragment, FC, useState, useEffect } from "react";
import Select, { ActionMeta, components, InputActionMeta } from "react-select";
import style from "./Select.module.scss";
import Image from "next/image";
import DropdownArrow from "@/assets/dropdown-arrow.png";

type Props = {
    data: [];
    name: string;
    onChange: any;
    value: any;
    styles?: any;
    className?: string;
    isClearable?: boolean;
    placeholder?: string;
};

const customStyles = {
    option: (provided: any, state: any) => ({
        ...provided,
        fontWeight: state.isSelected ? "bold" : "unset",
        padding: 5,
    }),
    singleValue: (provided: any, state: any) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = "opacity 300ms";

        return { ...provided, opacity, transition };
    },
    control: (provided: any, state: any) => ({
        ...provided,
        // height: "150px",
        fontSize: "calc(16px + (40 - 16) * ((100vw - 960px) / (3840 - 960)))",
        padding: "calc(10px + (30 - 10) * ((100vw - 960px) / (3840 - 960)))",
        fontWeight: "bold",
        borderRadius: "calc(8px + (25 - 8) * ((100vw - 360px) / (3840 - 360)))",
        backgroundColor: state.hasValue ? "#86C369" : "#FFF1D6",
        border: state.hasValue ? "solid calc(2px + (4 - 2) * ((100vw - 360px) / (3840 - 320))) #2b2b2b!important" : "solid calc(2px + (4 - 2) * ((100vw - 360px) / (3840 - 320))) #2b2b2b19!important",
    }),
    menu: (provided: any, state: any) => ({
        ...provided,
        zIndex: "2000",
    }),
    menuList: (provided: any, state: any) => ({
        ...provided,
        fontSize: "calc(14px + (40 - 14) * ((100vw - 960px) / (3840 - 960)))",
        height: "calc(200px + (300 - 200) * ((100vw - 960px) / (3840 - 960)))",
        zIndex: "3000"
    }),
    dropdownIndicator: (provided: any, state: any) => ({
        ...provided,
        width: "calc(35px + (55 - 35) * ((100vw - 960px) / (3840 - 960)))",
        height: "calc(35px + (55 - 35) * ((100vw - 960px) / (3840 - 960)))",
    }),
};

const DropdownIndicator = (props: any) => {
    return (
        <components.DropdownIndicator {...props}>
            <svg
                width="94"
                height="83"
                viewBox="0 0 94 83"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                style={{width: "100%", height: "100%"}}
            >
                <path
                    d="M51.3301 75.5C49.4056 78.8333 44.5944 78.8333 42.6699 75.5L6.2968 12.5C4.3723 9.16667 6.77792 5 10.6269 5L83.3731 4.99999C87.2221 4.99999 89.6277 9.16665 87.7032 12.5L51.3301 75.5Z"
                    fill={props.hasValue ? "#FDC54A" : "#FDC54Aaa"}
                    stroke={props.hasValue ? "#2b2b2b" : "#2b2b2baa"}
                    strokeWidth="10"
                />
            </svg>
        </components.DropdownIndicator>
    );
};

const FormSelect: FC<Props> = ({ children, ...pageProps }) => {
    const data = pageProps.data;
    const name = pageProps.name;
    const placeholder = pageProps.placeholder;

    return (
        <>
            <Select
                className={`${
                    pageProps.className
                        ? pageProps.styles[String(pageProps.className)]
                        : ""
                } ${"form-select-" + name}`}
                isClearable={pageProps.isClearable}
                isSearchable
                name={name}
                options={data}
                instanceId={name}
                inputId={name}
                onChange={pageProps.onChange}
                placeholder={placeholder ? placeholder : ""}
                components={{ DropdownIndicator }}
                value={
                    data[pageProps.value] !== undefined
                        ? data[pageProps.value]
                        : null
                }
                styles={customStyles}
            />
        </>
    );
};

export default FormSelect;
