import React, { FC, useEffect, useState } from "react";

import styles from "./Result.module.scss";

type Props = {
    children: any;
    pageName?: string;
};

const Result: FC<Props> = ({ children, pageName }) => {
    return (
        <div className={`${styles.container} ${pageName ? styles[pageName] : ""}`}>
            {children}
        </div>
    );
};

export default Result;
