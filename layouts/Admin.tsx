import Transition from "@/components/Transition";
import React, { FC } from "react";

import styles from "./Admin.module.scss";

const Admin: FC<{}> = ({ children }) => {
    return (
        <div className={styles.container}>
            <div className={styles.subcontainer}>
                {children}
            </div>
            <div className={styles.subcontainer_overlay}></div>
        </div>
    );
};

export default Admin;
