import Transition from "@/components/Transition";
import React, { FC } from "react";

import styles from "./Default.module.scss";

type Props = {
    children: any;
    pageName?: string;
};

const Default: FC<Props> = ({ children, pageName }) => {
    return (
        <div className={`${styles.container} ${pageName ? styles[pageName] : ""}`}>
            {children}
        </div>
    );
};

export default Default;
