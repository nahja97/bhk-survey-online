import { Button, Transition } from "@/components";
import Answer from "@/variables/identity.json";
import Image from "next/image";
import React, { Children, FC, useEffect, useState } from "react";

import styles from "./Identity.module.scss";

type Props = {
    children: any;
    pageName?: string;
};

const BtnBack = (props: any) => {
    const canBack = props.pageIndex > 0 ? true : false;
    const href = "/identities/" + Object.keys(Answer)[props.pageIndex - 1];
    if (canBack) {
        return (
            <Button href={href} type={"btnBack"}>
                <svg
                    width="143"
                    height="164"
                    viewBox="0 0 143 164"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M7.49998 86.3301C4.16666 84.4056 4.16665 79.5944 7.5 77.6699L130.5 6.65579C133.833 4.73129 138 7.13691 138 10.9859L138 153.014C138 156.863 133.833 159.269 130.5 157.344L7.49998 86.3301Z"
                        fill="#B3609E"
                        stroke="#2B2B2B"
                        strokeWidth="10"
                    />
                </svg>
            </Button>
        );
    } else {
        return <div style={{ display: "none" }}></div>;
    }
};

const BtnNext = (props: any) => {
    const canNext = props.pageIndex < 4 ? true : false;
    const href = "/identities/" + Object.keys(Answer)[props.pageIndex + 1];
    if (canNext) {
        if (props.local) {
            if (
                props.local[Object.keys(Answer)[props.pageIndex]] !== undefined
            ) {
                return (
                    <Button href={href} type={"btnNext"}>
                        <svg
                            width="143"
                            height="164"
                            viewBox="0 0 143 164"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M135.5 77.6699C138.833 79.5944 138.833 84.4056 135.5 86.3301L12.5 157.344C9.16663 159.269 4.99998 156.863 4.99998 153.014L4.99998 10.9859C4.99998 7.13691 9.16667 4.73129 12.5 6.65578L135.5 77.6699Z"
                                fill="#86C369"
                                stroke="#2B2B2B"
                                strokeWidth="10"
                            />
                        </svg>
                    </Button>
                );
            }
        }
    }

    return <div style={{ display: "none" }}></div>;
};

const Identity: FC<Props> = ({ children, pageName }) => {
    const [pageIndex, setPageIndex] = useState(0);
    const [local, setLocal] = useState([]);
    const [question, setQuestion] = useState(
        children.props.children.props.question
    );

    useEffect(() => {
        if (pageName) {
            var index = Object.keys(Answer).indexOf(pageName);
            setPageIndex(index);
            if (typeof window !== undefined) {
                const identitiesVal = localStorage.getItem("identity");
                if (identitiesVal) {
                    const identitiesParse = JSON.parse(identitiesVal);
                    setLocal(identitiesParse);
                }
            }
        }

        setQuestion(children.props.children.props.question);
    }, [pageName]);

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <BtnBack pageIndex={pageIndex} />
                {children}
                <BtnNext pageIndex={pageIndex} local={local} />
            </div>
            <div className={styles.content_overlay}></div>
        </div>
    );
};

export default Identity;
