import { Button, Transition } from "@/components";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { Children, FC, useEffect, useState } from "react";

import styles from "./Statement.module.scss";

type Props = {
    children: any;
    pageName?: string;
};

const Statement: FC<Props> = ({ children, pageName }) => {
    const router = useRouter();
    const pid = parseInt(router.route.substr(12, router.route.length));
    const [local, setLocal] = useState();
    const [progress, setProgress] = useState(0)

    useEffect(() => {
        if (typeof window !== undefined) {
            const statementsVal = localStorage.getItem("statements");
            if (statementsVal) {
                const identitiesParse = JSON.parse(statementsVal);
                setLocal(identitiesParse);
            }
        }
    }, []);

    useEffect(() => {
        setProgress((((pid/19)*100))-0.5)
        if (typeof window !== undefined) {
            const statementsVal = localStorage.getItem("statements");
            if (statementsVal) {
                const identitiesParse = JSON.parse(statementsVal);
                setLocal(identitiesParse);
            }
        }
    }, [router])

    const ProgressBar = () => {
        return (
            <div className={styles.progressbar}>
                <div className={styles.bar} style={{width: progress+"%", transition: "width 1s"}}></div>
            </div>
        )
    }

    const BtnBack = (props: any) => {
        const canBack = true;
        const href = pid == 0 ? "/identities/live" : "/statements/" + (pid - 1);
        if (canBack) {
            return (
                <Button href={href} type={"btnBack"}>
                    <svg
                        width="143"
                        height="164"
                        viewBox="0 0 143 164"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M7.49998 86.3301C4.16666 84.4056 4.16665 79.5944 7.5 77.6699L130.5 6.65579C133.833 4.73129 138 7.13691 138 10.9859L138 153.014C138 156.863 133.833 159.269 130.5 157.344L7.49998 86.3301Z"
                            fill="#B3609E"
                            stroke="#2B2B2B"
                            strokeWidth="10"
                        />
                    </svg>
                </Button>
            );
        } else {
            return <div style={{ display: "none" }}></div>;
        }
    };

    const BtnNext = (props: any) => {
        const canNext = pid < 19 ? true : false;
        const href = pid < 19 ? "/statements/" + (pid + 1) : "/result";
        if (canNext) {
            if (props.local) {
                if (props.local[pid] !== undefined) {
                    return (
                        <Button href={href} type={"btnNext"}>
                            <svg
                                width="143"
                                height="164"
                                viewBox="0 0 143 164"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M135.5 77.6699C138.833 79.5944 138.833 84.4056 135.5 86.3301L12.5 157.344C9.16663 159.269 4.99998 156.863 4.99998 153.014L4.99998 10.9859C4.99998 7.13691 9.16667 4.73129 12.5 6.65578L135.5 77.6699Z"
                                    fill="#86C369"
                                    stroke="#2B2B2B"
                                    strokeWidth="10"
                                />
                            </svg>
                        </Button>
                    );
                }
            }
        }

        return <div style={{ display: "none" }}></div>;
    };

    return (
        <>
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Shrikhand&display=swap"
                    rel="stylesheet"
                />
            </Head>
            <div className={styles.container}>
                <div className={styles.content}>
                    <BtnBack />
                    <Transition
                        styles={styles}
                        className={"question"}
                        exitType={"fade"}
                        pageName={"age"}
                    >
                        <h1>Apa yang Kamu Inginkan<br/>untuk Masa Depan Kotamu?</h1>
                    </Transition>
                    {children}
                    <ProgressBar />
                    <BtnNext local={local} />
                </div>
                <div className={styles.content_overlay}></div>
            </div>
        </>
    );
};

export default Statement;
