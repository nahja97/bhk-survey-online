import {
    ApolloProvider,
    ApolloClient,
    InMemoryCache,
    HttpLink,
    gql,
} from "@apollo/client";
import { useCookies } from "react-cookie";

function parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
        Buffer.from(base64, "base64")
            .toString()
            .split("")
            .map(function (c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
    );

    return JSON.parse(jsonPayload);
}

export async function Auth(req) {
    const client = new ApolloClient({
        link: new HttpLink({
            uri: "https://bukuhariankorona.org/graphql",
        }),
        cache: new InMemoryCache(),
    });

    const LoginMutation = gql`
        mutation LoginUser($input: LoginInput!) {
            login(input: $input) {
                authToken
                refreshToken
                user {
                    id
                    name
                }
            }
        }
    `;

    try {
        const { data } = await client.mutate({
            mutation: LoginMutation,
            variables: req.input,
        });

        return data;
    } catch (err) {
        return err;
    }
}

export function isSignedIn(cookies) {
    try {
        if (!cookies["access_token"]) {
            return false;
        }

        return true;
    } catch (err) {
        return false;
    }
}

export async function createSurvey(dataSurvey) {
    dataSurvey = JSON.stringify(dataSurvey)
    let token = ""
    const client = new ApolloClient({
        link: new HttpLink({
            uri: "https://bukuhariankorona.org/graphql",
        }),
        cache: new InMemoryCache(),
    });

    const LoginMutation = gql`
        mutation LoginUser {
            login(input: {username: "guest", password: "guest"}) {
                authToken
            }
        }
    `;

    try {
        const { data } = await client.mutate({
            mutation: LoginMutation
        });

        token = data.login.authToken

        const clientSurvey = new ApolloClient({
            link: new HttpLink({
                uri: "https://bukuhariankorona.org/graphql",
                headers: {
                    authorization: `Bearer ${token}`,
                },
            }),
            cache: new InMemoryCache(),
        });
        const create = gql`
            mutation MyMutation {
                createSurvey(input: {status: PUBLISH, content: ${dataSurvey}}) {
                    survey {
                        id
                    }
                }
            }
        `;
    
        try {
            const { data } = await clientSurvey.mutate({
                mutation: create
            });
    
            return data
        } catch (err) {
            console.log(err);
        }
    } catch (err) {
        console.log(err);
    }
}

export async function listSurvey(cookies, after = 0) {
    const client = new ApolloClient({
        link: new HttpLink({
            uri: "https://bukuhariankorona.org/graphql",
            // credentials: "include",
            headers: {
                authorization: `Bearer ${cookies["access_token"]}`,
            },
        }),
        cache: new InMemoryCache(),
    });

    const querySurveys = gql`
        query MyQuery {
            surveys (first: 1000000) {
                nodes {
                    id
                    content(format: RAW)
                }
            }
        }
    `;

    try {
        const { data } = await client.query({
            query: querySurveys
        });

        return data;
    } catch (err) {
        return err;
    }
}

export async function getSurveysAPI() {
    try {
        const res = await fetch('https://bukuhariankorona.org/wp-json/wp/v2/survey')
        console.log(await res.json())
    } catch(err) {
        console.log(err)
    }
}

export async function getSurvey(id) {
    const client = new ApolloClient({
        link: new HttpLink({
            uri: "https://bukuhariankorona.org/graphql"
        }),
        cache: new InMemoryCache(),
    });

    const querySurveys = gql`
        query survey {
            survey(id: "${id}") {
                content
            }
        }
    `;

    try {
        const { data } = await client.query({
            query: querySurveys
        });

        return data.survey;
    } catch (err) {
        return err;
    }
}
