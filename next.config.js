/** @type {import('next').NextConfig} */
const path = require('path')
const webpack = require("webpack");
const withPWA = require('next-pwa')
const runtimeCaching = require('next-pwa/cache')
const prod = process.env.NODE_ENV === 'production'

module.exports = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  webpack5: false,
  webpack(config, options) {
    if (!options.isServer) {
      config.node = {
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty',
      }
    }
    
    config.resolve.modules.push(path.resolve("./"));
    return config;
  },
  images: {
    loader: 'cloudinary',
    path: 'https://res.cloudinary.com/diyrn6ipx',
    // deviceSizes: [600, 640, 750, 828, 1080, 1200, 1920, 2048, 3840],
  },
  async redirects() {
    return [
      {
        source: '/data-survei',
        destination: '/data-survei/login',
        permanent: true,
      },
    ]
  },
  // pwa: {
  //   dest: 'public',
  //   runtimeCaching,
  //   disable: prod ? false : true
  // },
  trailingSlash: prod ? true : false,
  env: {
    apiKey: process.env.SHEET_KEY,
    clientId: process.env.CLIENT_ID,
    discoveryDocs: 
      ["https://sheets.googleapis.com/$discovery/rest?version=v4"],
    spreadsheetId: process.env.SPREAD_ID,
    clientEmail: process.env.CLIENT_EMAIL,
    clientKey: process.env.CLIENT_KEY
  }
}
